let cliente = JSON.parse(localStorage.getItem("cliente"));
if (cliente != null && cliente["nombres"]) {
    let userLocaStorage1 = document.getElementById('userLocaStorage1');
    let userLocaStorage2 = document.getElementById('userLocaStorage2');
    userLocaStorage1.innerHTML = '<Strong>' + cliente["nombres"] + '</Strong>';
    userLocaStorage2.innerHTML = '<Strong>' + cliente["nombres"] + '</Strong>';
} else {
    let btnIngresarComo = document.getElementById('btnIngresarComo');
    btnIngresarComo.value = 'ingresar';
    location.href = 'formLogin.html';
}
"use strict";
class AdminTS {
    constructor() {
        let btnIngresarComo = document.getElementById("btnIngresarComo");
        if (btnIngresarComo) {
            btnIngresarComo.addEventListener("click", (ev) => this.getTrainingNameOld(ev));
        }
        let btnIngresaNuevo = document.getElementById("btnIngresaNuevo");
        if (btnIngresaNuevo) {
            btnIngresaNuevo.addEventListener("click", (ev) => this.getTrainingNameNew(ev));
        }
    }
    getTrainingNameOld(ev) { //YA TIENE DATOS EN LOCALSTORAGE
        ev.preventDefault();
        ev.stopPropagation();
        //Se recupera información del formulario para enviar a registrar
        //const username = encriptarAtributo('josue.gonzales@seratic.com');
        //const password = encriptarAtributo('Black2022!');
        validSession('josue.gonzales@seratic.com', 'Black2022!');
    }
    getTrainingNameNew(ev) { //NUEVO SUSARIO EN LOCALSTORAGE
        ev.preventDefault();
        ev.stopPropagation();
        location.href = 'formLogin.html';
    }
}
new AdminTS();