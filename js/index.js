"use strict";
class AdminTS {
    constructor() {
        let btn = document.getElementById("btnRegistrar");
        if (btn) {
            btn.addEventListener("click", (ev) => this.getTrainingName(ev));
        }
    }
    getTrainingName(ev) {
        ev.preventDefault();
        location.href = 'formGetCode.html';
    }
}
// start the app
new AdminTS();