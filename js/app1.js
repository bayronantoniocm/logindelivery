"use strict";
class PageOne {
    constructor() {
        let btn = document.getElementById("btnEnviarSMs");
        let msgErrorMovil = document.getElementById("msgError");
        msgErrorMovil.innerHTML = '';
        let txtMovil = document.getElementById("txtNumber");
        if (txtMovil) {
            txtMovil.addEventListener('keyup', (ev) => {
                if (!this.validateNumberMovil(txtMovil.value)) {
                    txtMovil.classList.remove('custom-text-form');
                    txtMovil.classList.add('custom-error');
                    msgErrorMovil.innerHTML = 'Ingrese un número movil valido';
                } else {
                    txtMovil.classList.remove('custom-error');
                    txtMovil.classList.add('custom-text-form');
                    msgErrorMovil.innerHTML = '';
                }
            });
        }
        if (btn) {
            btn.addEventListener("click", (e) => this.getTrainingName(e));
        }
    }
    getTrainingName(ev) {
        ev.preventDefault();
        ev.stopPropagation();
        let form = document.getElementById('myForm');
        let msgErrorMovil = document.getElementById("msgError");
        let txtMovil = document.getElementById("txtNumber");
        let loading = document.getElementById("loading");
        let acceptSMS = document.getElementById("acetpMsm");
        console.log('Formulario', form.checkValidity());
        if (form.checkValidity()) {
            let accept = document.getElementById("chkAccept");
            if (accept) {
                let number = document.getElementById("txtNumber");
                console.log('numeroTXT ', number);
                if (accept.checked && number.value && number.value.trim().length > 0) {
                    // Todo generate, save code and send sms
                    const code = randomInteger(1000000);
                    console.log(code, 'numero ', number);
                    loading.style.display = "flex";
                    sendSMS(code, number.value)
                } else {
                    // Todo msg debe aceptar 
                    acceptSMS.style.color = "red";
                }
                console.log('value chk', accept.checked);
            }
        } else {
            txtMovil.classList.remove('custom-text-form');
            txtMovil.classList.add('custom-error');
            msgErrorMovil.innerHTML = 'EL número movil es obligatorio';
        }
    }
    validateNumberMovil(value) {
        var reg = /^\d+$/;
        return reg.test(value);
    }
}
var randomInteger = function(pow) {
    return Math.floor(Math.random() * pow);
};

function sendSMS(code, tel) {
    const xmlhttp = new XMLHttpRequest(); // new HttpRequest instance 
    const url = "https://ema2apploadermovildesa.latinapps.co/apploadermovil/licenses/enviarMensaje";
    xmlhttp.open("POST", url);
    xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xmlhttp.setRequestHeader('Access-Control-Allow-Origin', '*');
    xmlhttp.setRequestHeader('Access-Control-Allow-Headers', '*');
    xmlhttp.setRequestHeader('Access-Control-Allow-Credentials', 'true');
    xmlhttp.onreadystatechange = (ev) => {
        if (xmlhttp.readyState == 4) {
            if (xmlhttp.status == 200) {
                const resp = JSON.parse(xmlhttp.responseText);
                console.log(resp);
                const sms = { 'mensaje': code.toString(), 'movil': tel };
                localStorage.setItem('sms', JSON.stringify(sms));
                localStorage.setItem('token', "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlcyI6ImRlc2FfIiwidmVyc2lvbiI6IjU2OCIsImlkIjoiM2ZiZWI4MTNlYzUyZjBmMGVhMWE5MmU2YjMzMDIyMzY3Y2Y2N2FkNGE5YmM3ODBlNmFjNTBjNjA4ZjkwMzY5ZSJ9.9bbhl5f1PANtG5sVSqPWU4mHIJIpEWFaGlh4bjkwnTA");
                location.href = 'formValidatorCodeMsm.html';
                // TODO redirect next page
            } else
                console.log('Error loading page\n');
        }
    };
    xmlhttp.send(JSON.stringify({ 'mensaje': code.toString(), 'movil': tel }));
}
new PageOne();