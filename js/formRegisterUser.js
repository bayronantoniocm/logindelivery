"use strict";
class AdminTS {
    constructor() {
        let btn = document.getElementById("btnRegister");
        let txtName = document.getElementById("name");
        if (txtName) {
            txtName.addEventListener('keyup', (ev) => {
                validateName();
            });
        }
        let txtLastName = document.getElementById("lastname");
        if (txtLastName) {
            txtLastName.addEventListener('keyup', (ev) => {
                validateLastName();
            });
        }
        let txtEmail = document.getElementById("email");
        if (txtEmail) {
            txtEmail.addEventListener('keyup', (ev) => {
                validateEmail();
            });
        }
        let txtPassword = document.getElementById("passUser");
        if (txtPassword) {
            txtPassword.addEventListener('keyup', (ev) => {
                validatePassword();
            });
        }
        if (btn) {
            btn.addEventListener("click", (ev) => this.getTrainingName(ev));
        }
    }
    getTrainingName(ev) {
        ev.preventDefault();
        ev.stopPropagation();
        //Se recupera información del formulario para enviar a registrar
        let nameUser = document.getElementById("name").value;
        let lastnameUser = document.getElementById("lastname").value;
        let emailUser = document.getElementById("email").value;
        let passUser = document.getElementById("passUser").value;
        let sms = JSON.parse(localStorage.getItem("sms"));
        if (validateForm()) {
            const dataUser = {
                "nombres": nameUser,
                "apellidos": lastnameUser,
                "correo": emailUser,
                "username": emailUser,
                "password": passUser,
                "estado": true,
                "telefono": sms["movil"],
                "perfil": {
                    "nombre": "cliente"
                }
            };
            registerUser(dataUser);
            //const username = encriptarAtributo('josue.gonzales@seratic.com');
            //const password = encriptarAtributo('Black2022!');
            validSession('josue.gonzales@seratic.com', 'Black2022!');
        }
    }
}
new AdminTS();


/*Se procede a registrar el usuario en GO */
function registerUser(dataUser) {
    let loading = document.getElementById("loading");
    loading.style.display = "flex";
    const objectUser = { "usuario": dataUser };
    const bodyRegister = { "Objetos": [objectUser] };
    const xmlhttp = new XMLHttpRequest();
    const url = "https://ema2writerdesa.latinapps.co/apiwrite/insertregisterpost";
    xmlhttp.open("POST", url);
    xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xmlhttp.setRequestHeader("token", localStorage.getItem("token"));
    xmlhttp.send(JSON.stringify(bodyRegister));
    xmlhttp.onreadystatechange = (ev) => {
        if (xmlhttp.readyState == 4) {
            if (xmlhttp.status == 200) {
                localStorage.setItem("cliente", JSON.stringify(dataUser));
                let errormsm = document.getElementById('msm-error-register');
                if (errormsm.style.visibility == 'visible') {
                    errormsm.style.display = 'none'
                }
                let successmsm = document.getElementById('success-register');
                if (successmsm.style.display == '' || successmsm.style.display == 'none') {
                    successmsm.style.visibility = 'visible';
                }
                location.href = 'index.html';
            } else {
                loading.style.display = "none";
                let successmsm = document.getElementById('success-register');
                if (successmsm.style.visibility == 'visible') {
                    successmsm.style.display = 'none'
                }
                let errormsm = document.getElementById('msm-error-register');
                if (errormsm.style.display == '' || errormsm.style.display == 'none') {
                    errormsm.style.visibility = 'visible';
                    if (JSON.parse(xmlhttp.response)[0]) {
                        errormsm.innerHTML = '<strong>Error en servidor</strong> - excepción : ' +
                            JSON.parse(xmlhttp.response)[0].message;
                    }
                }
            }
        }
    };
}


function validateName() {
    let msgErrorName = document.getElementById("msgErrorName");
    let txtName = document.getElementById("name");
    if (txtName.value && txtName.value.trim().length > 0) {
        txtName.classList.remove('input-field-error');
        txtName.classList.add('input-field');
        msgErrorName.innerHTML = '';
        return true;
    } else {
        txtName.classList.remove('input-field');
        txtName.classList.add('input-field-error');
        msgErrorName.innerHTML = 'El nombre es obligatorio';
        return false;
    }
}

function validateLastName() {
    let msgErrorLastName = document.getElementById("msgErrorLastName");
    let txtLastName = document.getElementById("lastname");
    if (txtLastName.value && txtLastName.value.trim().length > 0) {
        txtLastName.classList.remove('input-field-error');
        txtLastName.classList.add('input-field');
        msgErrorLastName.innerHTML = '';
        return true;
    } else {
        txtLastName.classList.remove('input-field');
        txtLastName.classList.add('input-field-error');
        msgErrorLastName.innerHTML = 'El apellido es obligatorio';
        return false;
    }
}


function validatePassword() {
    let msgErrorPass = document.getElementById("msgErrorPass");
    let txtPasword = document.getElementById("passUser");
    if (txtPasword.value && txtPasword.value.trim().length > 0) {
        txtPasword.classList.remove('input-field-error');
        txtPasword.classList.add('input-field');
        msgErrorPass.innerHTML = '';
        return true;
    } else {
        txtPasword.classList.remove('input-field');
        txtPasword.classList.add('input-field-error');
        msgErrorPass.innerHTML = 'La contraseña es obligatorio';
        return false;
    }
}


function validateEmail() {
    let msgErrorEmail = document.getElementById("msgErrorEmail");
    let txtEmail = document.getElementById("email");
    if (txtEmail.value && txtEmail.value.trim().length > 0) {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(txtEmail.value)) {
            txtEmail.classList.remove('input-field-error');
            txtEmail.classList.add('input-field');
            msgErrorEmail.innerHTML = '';
            return true;
        } else {
            txtEmail.classList.remove('input-field');
            txtEmail.classList.add('input-field-error');
            msgErrorEmail.innerHTML = 'El correo es incorrecto';
            return false;
        }
    } else {
        txtEmail.classList.remove('input-field');
        txtEmail.classList.add('input-field-error');
        msgErrorEmail.innerHTML = 'El correo es obligatorio';
        return false;
    }
}


function validateForm() {
    validateName();
    validateLastName();
    validateEmail();
    validatePassword();
    if (validateName() && validateLastName() && validateEmail() && validatePassword()) {
        return true;
    } else {
        return false;
    }
}


function encriptarAtributo(message) {
    var key = "u/Gu5posvwDsXUnV5Zaq4g==",
        iv = "5D9r9ZVzEYYgha93/aUK2w==";

    key = CryptoJS.enc.Base64.parse(key);
    iv = CryptoJS.enc.Base64.parse(iv);

    var resultado = CryptoJS.AES.encrypt(message, key, { iv: iv });
    return resultado.toString();
}


function desencriptarAtributo(text) {
    var encrypted = CryptoJS.enc.Base64.parse(text);
    var key = CryptoJS.enc.Base64.parse('u/Gu5posvwDsXUnV5Zaq4g==');
    var iv = CryptoJS.enc.Base64.parse('5D9r9ZVzEYYgha93/aUK2w==');
    var resultado = CryptoJS.enc.Utf8.stringify(CryptoJS.AES.decrypt({ ciphertext: encrypted }, key, { mode: CryptoJS.mode.CBC, iv: iv }));
    return resultado;
}


function validSession(username, password) {
    let loading = document.getElementById("loading");
    loading.style.display = "flex";
    const url = 'https://releasetdp.latinapps.co/sesioncontroller/validar';
    const xmlhttp = new XMLHttpRequest();

    var formData = new FormData();

    formData.append("username", username);
    formData.append("password", password);
    formData.append("remember", true);
    formData.append("isAdmin", true);

    xmlhttp.open("POST", url);
    xmlhttp.setRequestHeader('Access-Control-Allow-Origin', '*');
    xmlhttp.setRequestHeader('Access-Control-Allow-Headers', '*');
    xmlhttp.setRequestHeader('Access-Control-Allow-Credentials', 'true');
    xmlhttp.onreadystatechange = (ev) => {
        if (xmlhttp.readyState == 4) {
            if (xmlhttp.status == 200) {
                loading.style.display = "none";
                const resp = JSON.parse(xmlhttp.responseText);
                window.parent.postMessage(resp, '*');
                console.log('SESSION', resp);
            } else {
                loading.style.display = "none";
                console.log('Error loading page\n');
            }
        }
    };
    xmlhttp.send(formData);
}