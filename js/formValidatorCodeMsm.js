let sms = JSON.parse(localStorage.getItem("sms"));
document.getElementById("lbl-phone").innerHTML = sms["movil"];

"use strict";
class AdminTS {
    constructor() {
        /*  CODIGO PARA ACTIVAR NEXT IMPUT AUTOMATICO
        var container = document.getElementsByClassName("container-code")[0];
        container.onkeyup = function(e) {
            var target = e.srcElement;
            var maxLength = parseInt(target.attributes["maxlength"].value, 10);
            var myLength = target.value.length;
            if (myLength >= maxLength) {
                var next = target;
                while (next = next.nextElementSibling) {
                    if (next == null)
                        break;
                    if (next.tagName.toLowerCase() == "input") {
                        next.focus();
                        break;
                    }
                }
            }
        }*/
        let btn = document.getElementById("btnValidar");
        if (btn) {
            btn.addEventListener("click", (ev) => this.getTrainingName(ev));
        }
    }
    getTrainingName(ev) {
        ev.preventDefault();
        ev.stopPropagation();
        let codeReal = sms["mensaje"]; //AQUI VA EL SERVICIO PARA VERIFICAR SI EL CODIGO DEL sms ESTA CORRECTO
        //Se obtiene el valor del código ingresado y se procede a validarlo
        let smsCodereceived = document.getElementById("codereceived").value;
        let codeReceivedsms = smsCodereceived;
        if (codeReceivedsms == codeReal) {
            location.href = 'formRegisterUser.html';
        } else {
            let msgErrorCodeSMS = document.getElementById("msgErrorCodeSMS");
            let txtCode = document.getElementById("codereceived");
            txtCode.classList.remove('input-field');
            txtCode.classList.add('input-field-error');
            msgErrorCodeSMS.innerHTML = 'El código no es correcto';
        }

    }
}
new AdminTS();