"use strict";
class AdminTS {
    constructor() {
        let btn = document.getElementById("btnIngresar");
        if (btn) {
            btn.addEventListener("click", (ev) => this.getTrainingName(ev));
        }
    }
    getTrainingName(ev) {
        ev.preventDefault();
        ev.stopPropagation();
        //Se recupera información del formulario para iniciar sesión
        let emailUser = document.getElementById("email").value;
        let passUser = document.getElementById("passUser").value;

        if (validateForm()) {
            if (passUser == 'Black2022!' && emailUser == 'josue.gonzales@seratic.com') {
                //const username = encriptarAtributo('soporte@myapps.pe');
                //const password = encriptarAtributo('OwnerED#11');
                //validSession(username, password);
                //loading.style.display = "flex";
                validSession('josue.gonzales@seratic.com', 'Black2022!');
            } else {
                console.log("ERROR CREDENCIALES");
                let errormsm = document.getElementById('msm-error-register');
                if (errormsm.style.display == '' || errormsm.style.display == 'none') {
                    errormsm.style.visibility = 'visible';
                    errormsm.innerHTML = '<strong>Accesos incorrectos</strong>';
                }
            }
        }
    }
}
new AdminTS();


function validateForm() {
    validateEmail();
    validatePassword();
    if (validateEmail() && validatePassword()) {
        return true;
    } else {
        return false;
    }
}


function validatePassword() {
    let msgErrorPass = document.getElementById("msgErrorPass");
    let txtPasword = document.getElementById("passUser");
    if (txtPasword.value && txtPasword.value.trim().length > 0) {
        txtPasword.classList.remove('input-field-error');
        txtPasword.classList.add('input-field');
        msgErrorPass.innerHTML = '';
        return true;
    } else {
        txtPasword.classList.remove('input-field');
        txtPasword.classList.add('input-field-error');
        msgErrorPass.innerHTML = 'La contraseña es obligatorio';
        return false;
    }
}


function validateEmail() {
    let msgErrorEmail = document.getElementById("msgErrorEmail");
    let txtEmail = document.getElementById("email");
    if (txtEmail.value && txtEmail.value.trim().length > 0) {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(txtEmail.value)) {
            txtEmail.classList.remove('input-field-error');
            txtEmail.classList.add('input-field');
            msgErrorEmail.innerHTML = '';
            return true;
        } else {
            txtEmail.classList.remove('input-field');
            txtEmail.classList.add('input-field-error');
            msgErrorEmail.innerHTML = 'El correo es incorrecto';
            return false;
        }
    } else {
        txtEmail.classList.remove('input-field');
        txtEmail.classList.add('input-field-error');
        msgErrorEmail.innerHTML = 'El correo es obligatorio';
        return false;
    }
}


/*funcion de encriptar accesos para poder descifrar en el serrvicio*/
function encriptarAtributo(message) {
    var key = "u/Gu5posvwDsXUnV5Zaq4g==",
        iv = "5D9r9ZVzEYYgha93/aUK2w==";

    key = CryptoJS.enc.Base64.parse(key);
    iv = CryptoJS.enc.Base64.parse(iv);

    var resultado = CryptoJS.AES.encrypt(message, key, { iv: iv });
    return resultado.toString();
}


function desencriptarAtributo(text) {
    var encrypted = CryptoJS.enc.Base64.parse(text);
    var key = CryptoJS.enc.Base64.parse('u/Gu5posvwDsXUnV5Zaq4g==');
    var iv = CryptoJS.enc.Base64.parse('5D9r9ZVzEYYgha93/aUK2w==');
    var resultado = CryptoJS.enc.Utf8.stringify(CryptoJS.AES.decrypt({ ciphertext: encrypted }, key, { mode: CryptoJS.mode.CBC, iv: iv }));
    return resultado;
}


function validSession(username, password) {
    let loading = document.getElementById("loading");
    loading.style.display = "flex";
    const url = 'https://releasetdp.latinapps.co/sesioncontroller/validar';
    const xmlhttp = new XMLHttpRequest();

    var formData = new FormData();

    formData.append("username", username);
    formData.append("password", password);
    formData.append("remember", true);
    formData.append("isAdmin", true);

    xmlhttp.open("POST", url);
    xmlhttp.setRequestHeader('Access-Control-Allow-Origin', '*');
    xmlhttp.setRequestHeader('Access-Control-Allow-Headers', '*');
    xmlhttp.setRequestHeader('Access-Control-Allow-Credentials', 'true');
    xmlhttp.onreadystatechange = (ev) => {
        if (xmlhttp.readyState == 4) {
            if (xmlhttp.status == 200) {
                loading.style.display = "none";
                const resp = JSON.parse(xmlhttp.responseText);
                window.parent.postMessage(resp, '*');
                console.log('SESSION', resp);
            } else {
                loading.style.display = "none";
                console.log('Error loading page\n');
            }
        }
    };
    xmlhttp.send(formData);
}